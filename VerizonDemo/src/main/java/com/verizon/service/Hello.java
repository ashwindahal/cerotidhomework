package com.verizon.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {
	
	@GetMapping("/hello")
	public String greet(@RequestParam(name="name", required = true, defaultValue = "SomeName") String name) {
		return "Hello," + name;
	}
}
