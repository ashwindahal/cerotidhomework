package com.verizon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerizonDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerizonDemoApplication.class, args);
	}

}
